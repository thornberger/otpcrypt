package encoder

import (
	"errors"
	"github.com/onsi/gomega"
	"testing"
)

type TestCase struct {
	expected      string
	expectedError error
	text          string
}

var checkerboard = Checkerboard{
	letters: map[string]string{
		"\n": "80",
		"be": "51",
		"ch": "53",
		"de": "55",
		"ge": "58",
		"te": "71",
		"un": "73",
		"a":  "0",
		"e":  "1",
		"i":  "2",
		"n":  "3",
		"r":  "4",
		"b":  "50",
		"c":  "52",
		"d":  "54",
		"f":  "56",
		"g":  "57",
		"h":  "59",
		"j":  "60",
		"k":  "61",
		"l":  "62",
		"m":  "63",
		"o":  "64",
		"p":  "67",
		"q":  "68",
		"s":  "69",
		"t":  "70",
		"u":  "72",
		"v":  "74",
		"w":  "76",
		"x":  "77",
		"y":  "78",
		"z":  "79",
		" ":  "83",
	},
	figures: map[string]string{
		".":  "89",
		":":  "90",
		",":  "91",
		"-":  "92",
		"/":  "93",
		"(":  "94",
		")":  "95",
		"+":  "96",
		"=":  "97",
		"\"": "98",
		"0":  "00",
		"1":  "11",
		"2":  "22",
		"3":  "33",
		"4":  "44",
		"5":  "55",
		"6":  "66",
		"7":  "77",
		"8":  "88",
		"9":  "99",
	},
	figureSymbol: "82",
	letterSymbol: "81",
	codeBook:     "99",
}
var codebook = map[string]string{}

var testCasesWithoutError = []TestCase{
	// simple text
	{
		expected: "696463170592357",
		text:     "something",
	},
	// text with double char substitution
	{
		expected: "51053",
		text:     "beach",
	},
	// text with spaces
	{
		expected: "59162626483766446254",
		text:     "hello world",
	},
}

var testCasesWithError = []TestCase{
	// undefined umlaut
	{
		expectedError: errors.New("Could not find a substitute for 'ü'"),
		text:          "drüben",
	},
}

func TestNewEncoder(t *testing.T) {
	gomega.RegisterTestingT(t)

	encoder := NewEncoder(checkerboard, codebook)

	gomega.Expect(encoder.checkerboard).Should(gomega.Equal(checkerboard))
	gomega.Expect(encoder.lengths).Should(gomega.Equal([]int{2, 1}))
}

func TestEncodeWithoutError(t *testing.T) {
	gomega.RegisterTestingT(t)

	encoder := NewEncoder(checkerboard, codebook)

	for _, tc := range testCasesWithoutError {
		result, err := encoder.Encode(tc.text)

		gomega.Expect(result.String()).To(gomega.Equal(tc.expected))
		gomega.Expect(err).To(gomega.BeNil(), "No error should occur")
	}
}

func TestEncodeWithError(t *testing.T) {
	gomega.RegisterTestingT(t)

	encoder := NewEncoder(checkerboard, codebook)

	for _, tc := range testCasesWithError {
		result, err := encoder.Encode(tc.text)

		gomega.Expect(result.String()).To(gomega.Equal(tc.expected))
		gomega.Expect(err).To(gomega.Equal(tc.expectedError))
	}
}

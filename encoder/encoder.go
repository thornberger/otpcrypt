package encoder

import (
	"bytes"
	"fmt"
	"sort"
	"unicode/utf8"
)

/**
* TODO:

messages maximum 250 digits
support all kinds of checkerboards from http://users.telenet.be/d.rijmenants/en/table.htm
http://scz.bplaced.net/m.html
support codebooks


*/

type Encoder struct {
	checkerboard Checkerboard
	codebook     map[string]string
	lengths      []int
}

func NewEncoder(checkerboard Checkerboard, codebook map[string]string) *Encoder {
	return &Encoder{
		checkerboard: checkerboard,
		codebook:     codebook,
		lengths:      getLengths(checkerboard, codebook),
	}
}

func (e *Encoder) Encode(message string) (bytes.Buffer, error) {
	var encoded bytes.Buffer
	n := len(message)
	m := len(e.lengths)
	followsLetter := true

	for i := 0; i < n; i++ {
		found := false

		for k := 0; k < m && !found; k++ {
			j := e.lengths[k]

			// ignore all substrings that are too long
			if i+j > n {
				continue
			}

			var sub string

			sub, found = e.substituteWord(string(message[i : i+j]))
			if !found {
				sub, found = e.substituteLetter(string(message[i : i+j]))
				if found && !followsLetter {
					sub = e.checkerboard.letterSymbol + sub
					followsLetter = false
				}

				if !found && j == 1 {
					sub, found = e.substituteFigure(string(message[i : i+j]))
					if found && followsLetter {
						sub = e.checkerboard.figureSymbol + sub
						followsLetter = false
					}
				}
			}

			if found {
				encoded.WriteString(sub)
				i = i + j - 1
			}
		}

		if !found {
			r, _ := utf8.DecodeRuneInString(message[i:])
			return bytes.Buffer{}, fmt.Errorf("Could not find a substitute for '%s'", string(r))
		}
	}

	return encoded, nil
}

func (e *Encoder) substituteWord(x string) (string, bool) {
	if r, ok := e.checkerboard.words[x]; ok {
		return r, true
	}
	return "", false
}

func (e *Encoder) substituteLetter(x string) (string, bool) {
	if r, ok := e.checkerboard.letters[x]; ok {
		return r, true
	}
	return "", false
}

func (e *Encoder) substituteFigure(x string) (string, bool) {
	if r, ok := e.checkerboard.figures[x]; ok {
		return r, true
	}
	return "", false
}

func getLengths(cb Checkerboard, co map[string]string) []int {
	l := make(map[int]struct{})

	addKeyLengths(l, co)
	addKeyLengths(l, cb.words)
	addKeyLengths(l, cb.letters)
	addKeyLengths(l, cb.figures)

	var r []int
	for k := range l {
		r = append(r, k)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(r)))

	return r
}

func addKeyLengths(lengths map[int]struct{}, table map[string]string) {
	for key := range table {
		kLen := len(key)
		if _, ok := lengths[kLen]; !ok {
			lengths[kLen] = struct{}{}
		}
	}
}

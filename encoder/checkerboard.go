package encoder

type Checkerboard struct {
	words        map[string]string `json:"words,omitempty"`
	letters      map[string]string `json:"letters"`
	figures      map[string]string `json:"figures"`
	figureSymbol string            `json:"figure_symbol"`
	letterSymbol string            `json:"letter_symbol"`
	codeBook     string            `json:"code_book,omitempty"`
	filler       string            `json:"filler"`
}
